vim.api.nvim_buf_set_keymap(
    0,
    "n",
    "gf",
    '<Cmd>lua require("mdfollow").follow()<CR>',
    { noremap = true, silent = true }
)
