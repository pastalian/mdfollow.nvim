local ts_utils = require("nvim-treesitter.ts_utils")
local M = {}

local function get_link()
    local node = ts_utils.get_node_at_cursor()

    if not node then
        return
    end

    local tnode = node
    local ntype = node:type()

    if ntype == "inline_link" then
        tnode = node:named_child(1) or node:named_child(0)
    elseif ntype == "link_text" then
        tnode = node:next_named_sibling()
    elseif ntype == "wiki_link" then
        tnode = node:named_child(0)
        return vim.treesitter.get_node_text(tnode, 0) .. ".md"
    elseif ntype ~= "link_destination" then
        return
    end

    if node:parent():type() == "wiki_link" then
        return vim.treesitter.get_node_text(tnode, 0) .. ".md"
    end

    return vim.treesitter.get_node_text(tnode, 0)
end

local function is_url(link)
    if link:match("^https?:") then
        return true
    end

    return false
end

function M.follow()
    local link = get_link()

    if not link then
        return
    end

    if is_url(link) then
        vim.fn.jobstart({ "xdg-open", link }, { detach = true })
        return
    end

    local alink = link

    if link:sub(1, 1) == "#" then
        local root = vim.treesitter.get_parser():parse()[1]:root()

        local query = vim.treesitter.query.parse("markdown", "(atx_heading) @atx_heading")
        local last = vim.api.nvim_buf_line_count(0)
        local iter = query:iter_captures(root, 0, 0, last + 1)

        for _, node, _ in iter do
            print(vim.inspect({ ts_utils.get_vim_range({ ts_utils.get_node_range(node) }, 0) }))
        end
    end

    if link:sub(1, 1) ~= "/" then
        local f = vim.fs.find(link)
        if next(f) ~= nil then
            alink = f[1]
        else
            -- Assuming file/directory name is unique.
            local d = vim.fs.find(link:sub(1, -4))
            if vim.fn.isdirectory(d[1]) == 1 then
                alink = d[1]
            else
                alink = vim.fn.expand("%:p:h") .. "/" .. link
            end
        end
    end

    if vim.fn.isdirectory(alink) == 1 then
        if alink:sub(-1) == "/" then
            alink = alink:sub(1, -2)
        end

        alink = alink .. "/" .. "index.md"
    end

    vim.cmd("edit " .. alink)
end

return M
